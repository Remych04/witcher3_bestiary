import bears from "./monsters/bears.webp";
import bigBadWolf from "./monsters/big-bad-wolf.webp";
import dogs from "./monsters/dogs.webp";

type ImageMap = {
  [key: string]: string;
};

const imageMap: ImageMap = {
  "Bears": bears,
  "Big Bad Wolf": bigBadWolf,
  "Dogs": dogs,
};

type Props = {
  name: string;
};

export function useStaticImport({ name }: Props) {
  const imageSrc = imageMap[name] || "";
  return { imageSrc };
}

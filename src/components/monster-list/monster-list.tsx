import { Monster } from "../monster/monster";
import { useRecoilValue } from "recoil";
import { filteredMonsterListState } from "../../states/filters/selectors";
import { Grid } from "@mantine/core";

export function MonsterList() {
  const filteredMonsters = useRecoilValue(filteredMonsterListState);

  return (
    <Grid gutter="md" justify="flex-start" align="stretch">
      {filteredMonsters.map((monster) => (
        <Grid.Col key={monster.name} lg={6} md={6}>
          <Monster
            name={monster.name}
            edition={monster.edition}
            susceptibility={monster.susceptibility}
          />
        </Grid.Col>
      ))}
    </Grid>
  );
}

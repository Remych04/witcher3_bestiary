import { useSetRecoilState } from "recoil";
import { filterListState } from "../../../states/filters/atom";
import { Checkbox } from "@mantine/core";

export function SignFilters() {
  const setChecked = useSetRecoilState(filterListState);

  const handleChange = (filters: string[]) => {
    setChecked((map) => new Map(map).set("sign", filters));
  };

  return (
    <Checkbox.Group label="Signs" onChange={handleChange}>
      <Checkbox label="Aard" value="Aard" />
      <Checkbox label="Axii" value="Axii" />
      <Checkbox label="Igni" value="Igni" />
      <Checkbox label="Quen" value="Quen" />
      <Checkbox label="Yrden" value="Yrden" />
    </Checkbox.Group>
  );
}

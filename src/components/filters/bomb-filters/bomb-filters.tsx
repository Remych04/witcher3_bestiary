import { useSetRecoilState } from "recoil";
import { filterListState } from "../../../states/filters/atom";
import { Checkbox } from "@mantine/core";

export function BombFilters() {
  const setChecked = useSetRecoilState(filterListState);

  const handleChange = (filters: string[]) => {
    setChecked((map) => new Map(map).set("bomb", filters));
  };

  return (
    <Checkbox.Group label="Bombs" onChange={handleChange}>
      <Checkbox label="Dancing Star" value="Dancing Star" />
      <Checkbox label="Devil's Puffball" value="Devil's Puffball" />
      <Checkbox label="Dimeritium bomb" value="Dimeritium bomb" />
      <Checkbox label="Grapeshot" value="Grapeshot" />
      <Checkbox label="Moon dust" value="Moon dust" />
      <Checkbox label="Northern wind" value="Northern wind" />
      <Checkbox label="Samum" value="Samum" />
    </Checkbox.Group>
  );
}

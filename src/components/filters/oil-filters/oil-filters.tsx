import { useSetRecoilState } from "recoil";
import { filterListState } from "../../../states/filters/atom";
import { Checkbox } from "@mantine/core";

export function OilFilters() {
  const setChecked = useSetRecoilState(filterListState);

  const handleChange = (filters: string[]) => {
    setChecked((map) => new Map(map).set("oil", filters));
  };

  return (
    <Checkbox.Group label="Oils" onChange={handleChange}>
      <Checkbox label="Beast oil" value="Beast oil" />
      <Checkbox label="Cursed oil" value="Cursed oil" />
      <Checkbox label="Draconid oil" value="Draconid oil" />
      <Checkbox label="Elementa oil" value="Elementa oil" />
      <Checkbox label="Hybrid oil" value="Hybrid oil" />
      <Checkbox label="Insectoid oil" value="Insectoid oil" />
      <Checkbox label="Necrophage oil" value="Necrophage oil" />
      <Checkbox label="Ogroid oil" value="Ogroid oil" />
      <Checkbox label="Relict oil" value="Relict oil" />
      <Checkbox label="Specter oil" value="Specter oil" />
      <Checkbox label="Vampire oil" value="Vampire oil" />
    </Checkbox.Group>
  );
}

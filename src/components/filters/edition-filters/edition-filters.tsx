import { useSetRecoilState } from "recoil";
import { filterListState } from "../../../states/filters/atom";
import { Checkbox } from "@mantine/core";

export function EditionFilters() {
  const setChecked = useSetRecoilState(filterListState);

  const handleChange = (filters: string[]) => {
    setChecked((map) => new Map(map).set("edition", filters));
  };

  return (
    <Checkbox.Group label="Edition" onChange={handleChange}>
      <Checkbox label="Base" value="BASE" />
      <Checkbox label="Hearts of Stone" value="HOS" />
      <Checkbox label="Blood and Wine" value="BAW" />
      <Checkbox label="Patch v4.0" value="PATCH4" />
    </Checkbox.Group>
  );
}

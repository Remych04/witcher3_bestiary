import { useSetRecoilState } from "recoil";
import { filterListState } from "../../../states/filters/atom";
import { Checkbox } from "@mantine/core";

export function PotionFilters() {
  const setChecked = useSetRecoilState(filterListState);

  const handleChange = (filters: string[]) => {
    setChecked((map) => new Map(map).set("potion", filters));
  };

  return (
    <Checkbox.Group label="Potions" onChange={handleChange}>
      <Checkbox label="Blizzard" value="Blizzard" />
      <Checkbox label="Black Blood" value="Black Blood" />
      <Checkbox label="Golden Oriole" value="Golden Oriole" />
      <Checkbox label="White Honey" value="White Honey" />
      <Checkbox label="Reinald's Philter" value="Reinald's Philter" />
    </Checkbox.Group>
  );
}

import { EditionFilters } from "./edition-filters/edition-filters";
import { SignFilters } from "./sign-filters/sign-filters";
import { BombFilters } from "./bomb-filters/bomb-filters";
import { PotionFilters } from "./potion-filters/potion-filters";
import { OilFilters } from "./oil-filters/oil-filters";

export function Filters() {
  return (
    <div>
      <h5>Filters</h5>
      <EditionFilters />
      <SignFilters />
      <BombFilters />
      <PotionFilters />
      <OilFilters />
    </div>
  );
}

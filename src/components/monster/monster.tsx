import React from "react";
import { Card, Image, Text, Badge, Group, List, Stack } from "@mantine/core";

import styles from "./index.module.css";
import { useStaticImport } from "../../static/static-import.tsx";

type MonsterProps = {
  name: string;
  edition: string;
  susceptibility: string[];
};

function MonsterComponent({ name, edition, susceptibility }: MonsterProps) {
  const { imageSrc } = useStaticImport({ name });
  return (
    <Card
      className={styles.block}
      shadow="sm"
      padding="lg"
      radius="md"
      withBorder
    >
      <Card.Section>
        <Group spacing="xs" grow>
          <Image
            imageProps={{ loading: "lazy" }}
            withPlaceholder={true}
            fit={"cover"}
            src={imageSrc}
            height={300}
            alt={name}
          />
          <Stack align="flex-start" spacing="sm">
            <Text weight={500}>{name}</Text>
            <Badge color="pink" variant="light">
              {edition}
            </Badge>
            <List>
              {susceptibility.map((susceptibility, index) => (
                <List.Item key={index}>{susceptibility}</List.Item>
              ))}
            </List>
          </Stack>
        </Group>
      </Card.Section>
    </Card>
  );
}

export const Monster = React.memo(MonsterComponent);

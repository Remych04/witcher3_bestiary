import { useSetRecoilState } from "recoil";
import { monsterListState } from "../../states/filters/atom";
import { MonsterList } from "../monster-list/monster-list";
import { useEffect, useState } from "react";
import {
  AppShell,
  Burger,
  Header,
  MediaQuery,
  Navbar,
  ScrollArea,
  useMantineTheme,
} from "@mantine/core";
import { Filters } from "../filters/filters";
import { BestiaryJson } from "../../types/bestiary-types.ts";
import bestiaryJson from "../../mock/data/bestiary-list.json";

export function MainWrapper() {
  const theme = useMantineTheme();
  const [opened, setOpened] = useState(false);

  const setMonsterList = useSetRecoilState(monsterListState);

  useEffect(() => {
    const data: BestiaryJson = JSON.parse(JSON.stringify(bestiaryJson));
    setMonsterList(data.bestiaryList);
  });
  return (
    <AppShell
      navbarOffsetBreakpoint="sm"
      asideOffsetBreakpoint="sm"
      navbar={
        <Navbar
          p="md"
          hiddenBreakpoint="sm"
          hidden={!opened}
          width={{ sm: 200 }}
        >
          <ScrollArea scrollbarSize={4}>
            <Filters />
          </ScrollArea>
        </Navbar>
      }
      header={
        <Header height={{ base: 50 }} p="md">
          <div
            style={{ display: "flex", alignItems: "center", height: "100%" }}
          >
            <MediaQuery largerThan="sm" styles={{ display: "none" }}>
              <Burger
                opened={opened}
                onClick={() => setOpened((isOpen) => !isOpen)}
                size="sm"
                color={theme.colors.gray[6]}
                mr="xl"
              />
            </MediaQuery>
            The Witcher 3 Bestiary
          </div>
        </Header>
      }
    >
      <MonsterList />
    </AppShell>
  );
}

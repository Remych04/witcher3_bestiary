export type MonsterType = {
  type: string;
  name: string;
  edition: Edition;
  susceptibility: string[];
};

export type Edition = "BASE" | "HOS" | "BAW" | "PATCH4";

export type BestiaryJson = {
  bestiaryList: MonsterType[];
};

import { selector } from "recoil";
import { filterListState, monsterListState } from "./atom";
import { MonsterType } from "../../types/bestiary-types";

export const filteredMonsterListState = selector<MonsterType[]>({
  key: "FilteredMonsterList",
  get: ({ get }) => {
    const filterMap = get(filterListState);
    const monsterList = get(monsterListState);
    return monsterList.filter((monster) => {
      let isFilterInclude = true;

      filterMap.forEach((filter, key) => {
        if (key === "edition" && filter.length > 0) {
          isFilterInclude = isFilterInclude && filter.includes(monster.edition);
        } else if (key !== "edition" && filter.length > 0) {
          isFilterInclude =
            isFilterInclude &&
            filter.every((value) => monster.susceptibility.includes(value));
        }
      });

      return isFilterInclude;
    });
  },
});

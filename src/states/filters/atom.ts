import { atom } from "recoil";
import { MonsterType } from "../../types/bestiary-types";

type Filters = "edition" | "sign" | "bomb" | "potion" | "oil";

export const monsterListState = atom<MonsterType[]>({
  key: "MonsterList",
  default: [],
});

export const filterListState = atom<Map<Filters, string[]>>({
  key: "FilterList",
  default: new Map(),
});

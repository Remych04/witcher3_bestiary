import React from "react";
import ReactDOM from "react-dom/client";
import { MantineProvider } from "@mantine/core";
import { RecoilRoot } from "recoil";
import { MainWrapper } from "./components/main-wrapper/main-wrapper.tsx";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <RecoilRoot>
        <MainWrapper />
      </RecoilRoot>
    </MantineProvider>
  </React.StrictMode>,
);

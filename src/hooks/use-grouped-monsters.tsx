import { useMemo } from "react";
import { MonsterType } from "../types/bestiary-types";

type GroupedMonsters = Record<string, MonsterType[]>;

export function useGroupedMonsters(
  bestiaryList: MonsterType[],
): GroupedMonsters {
  return useMemo(() => {
    return bestiaryList.reduce((acc: GroupedMonsters, monster) => {
      if (!acc[monster.type]) {
        acc[monster.type] = [];
      }
      acc[monster.type].push(monster);
      return acc;
    }, {});
  }, [bestiaryList]);
}
